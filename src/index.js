import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


function Square(props) {
    return (
        <button className={"square" + (props.winner ? ' winner' : '')} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

const Board = ({ squares, winner, onClick }) => <div>
    {[0, 1, 2].map((row) => (
        <div key={row} className="board-row">
            {[0, 1, 2].map((col) => (row * 3 + col))
                .map((idx) => (
                    <Square key={idx}
                            value={squares[idx]}
                            winner={winner && winner.indexOf(idx) !== -1}
                            onClick={() => onClick(idx)}
                    />
                ))
            }
        </div>
    ))}
</div>;

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
                position: 0,
                stepNum: 0,
            }],
            xIsNext: true,
            stepNum: 0,
            sortReverse: false,
        }
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNum + 1);
        const squares = history[this.state.stepNum].squares.slice();

        if (calculateWinner(squares) || squares[i]) {
            return
        }

        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
                position: i,
                stepNum: history.length
            }]),
            xIsNext: !this.state.xIsNext,
            stepNum: history.length,
        })
    }

    jumpTo(step) {
        const history = this.state.history;

        this.setState({
            stepNum: step,
            xIsNext: (step % 2) === 0,
        })
    }

    sortMoves() {
        this.setState({
            sortReverse: !this.state.sortReverse,
        });
    }

    render() {
        const history = this.state.history;
        const winner = calculateWinner(history[this.state.stepNum].squares);
        let moves = history.map(( step, move) => {
            const desc = step.stepNum ?
                'Go to move ' + step.stepNum + ': position ' + getSquarePos(step.position)  :
                'Go to start of game';

            return (
                <li key={move}>
                    <button className={step.stepNum === this.state.stepNum ? 'bold' : ''} onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            )
        });

        if (this.state.sortReverse) {
            moves = moves.reverse();
        }

        let status;

        if (winner) {
            status = 'Winner is ' + winner.name;
        } else if (history.length === 10) {
            status = 'No one :(';
        } else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={history[this.state.stepNum].squares}
                        winner={winner ? winner.lines : null}
                        onClick={(i) => { this.handleClick(i)} }/>
                </div>
                <div className="game-info">
                    <div>{ status }</div>
                    <button onClick={()=>this.sortMoves()}>Sort</button>
                    <ol>{ moves }</ol>
                </div>
            </div>
        );
    }
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return {name: squares[a], lines: lines[i]};
        }
    }
    return null;
}

function getSquarePos(i) {
    const row = Math.floor(i / 3) + 1;
    const col = (i % 3) + 1;
    return [col, row]
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
